From: Sam James <sam@gentoo.org>
Date: Sat, 5 Nov 2022 06:04:27 +0000
Subject: build: fix -Wimplicit-int, -Wimplicit-function-declaration

Clang 16 will make -Wimplicit-int and -Wimplicit-function-declaration
errors by default.

In this case, it manifests as:
```
checking for data type to store Lisp values... configure: error: cannot find Lisp value type; set --with-value-type (see README)
```

For more information, see LWN.net [0] or LLVM's Discourse [1], or the
(new) c-std-porting mailing list [2].

[0] https://lwn.net/Articles/913505/
[1] https://discourse.llvm.org/t/configure-script-breakage-with-the-new-werror-implicit-function-declaration/65213
[2] hosted at lists.linux.dev.

Signed-off-by: Sam James <sam@gentoo.org>
Bug-Ubuntu: https://launchpad.net/bugs/2060791
Origin: https://gitweb.gentoo.org/repo/gentoo.git/commit/?id=a935681a6fcabf47524c72420050b6a9030f555d
---
 configure.in | 12 ++++++++----
 1 file changed, 8 insertions(+), 4 deletions(-)

diff --git a/configure.in b/configure.in
index 1d959f4..4448c45 100644
--- a/configure.in
+++ b/configure.in
@@ -432,15 +432,18 @@ AC_ARG_WITH(value-type,
 			   as a pointer. [TYPE=<probed>] (see README)], [],
  [with_value_type="undef"])
 if test "${with_value_type}" = "undef"; then
-  AC_TRY_RUN([main () { exit (!(sizeof (int) >= sizeof (void *)));}],
+  AC_TRY_RUN([#include <stdlib.h>
+             int main () { exit (!(sizeof (int) >= sizeof (void *)));}],
 	     [with_value_type=int])
 fi
 if test "${with_value_type}" = "undef"; then
-  AC_TRY_RUN([main () { exit (!(sizeof (long int) >= sizeof (void *)));}],
+  AC_TRY_RUN([#include <stdlib.h>
+             int main () { exit (!(sizeof (long int) >= sizeof (void *)));}],
 	     [with_value_type="long int"])
 fi
 if test "${with_value_type}" = "undef"; then
-  AC_TRY_RUN([main () { exit (!(sizeof (long long int) >= sizeof (void *)));}],
+  AC_TRY_RUN([#include <stdlib.h>
+             int main () { exit (!(sizeof (long long int) >= sizeof (void *)));}],
 	     [with_value_type="long long int"])
 fi
 if test "${with_value_type}" = "undef"; then
@@ -457,7 +460,8 @@ AC_ARG_WITH(value-sizeof,
 if test "${with_value_sizeof}" = "undef"; then
   dnl the following fragment is inspired by AC_CHECK_SIZEOF
   AC_TRY_RUN([#include <stdio.h>
-	      main () {
+		#include <stdlib.h>
+		int main () {
 		FILE *f = fopen ("conftestval", "w");
 		if (!f) exit (1);
 		fprintf (f, "%d\n", sizeof (${with_value_type}));
